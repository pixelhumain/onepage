
# Education
Territoire Qu’on emprunte à nos enfants

Les qualités d’un territoire et d’une société se révèle dans son éducation 
libre, créative, productive, artistique, et virale

## Valeurs 
- Empathie: développer l’empathie pour contribuer au vivre ensemble
- Vivre ensemble : moins de compétition 
- Le droit de copier 
- Collaborer , cocréer 
- Échanger, mutualiser 
- Interculturalité : nos différences sont nos richesses, 
- valoriser les spécificités culturelle de chacun, pour  découvrir l’autre
- Valoriser les valeurs communes
- Le libre arbitre, l’auto-détermination:  développer les capacités à se décider, à faire un choix.

## Nouvelles Methodes 
- Permettons donc à l'intelligence d'explorer et de réaliser des milliards de connexions
- Adopter une approche interdisciplinaire et transversale pour retrouver du sens aux apprentissages
- Favoriser l’autonomie et  l’expérimentation,  l’expression du SOI,
- Mixité générationnelle dans les classes pour multiplier le champ des possibles en terme d’exemplarité
- La médiation par les pairs, donner les outils aux enfants pour leur permettre de désamorcer des situations de conflits: apprentissage du langage émotionnel, et culturel.

## Mise en application 
- Apprentissage de la coopération: Freinet (1896-1966)
- Apprentissage à partir du réel vers le réel: Decroly (1871-1932)
- Pédagogie reposant sur l’éducation sensorielle: Montessori (1870-1952)
- Céline Alvarez : Les lois naturelles de l’enfant ( 2016)

## Attention
- Au colonialisme culturel 
- Trop d’info tue : la créativité, esprit critique, la volonté, libre arbitre…
- L’exposition à la violence et le manque d'interactions
- Respecter : spiritualité laïque 

## Outils
- Formation sur les pratiques de l'économie circulaire 
- Dés le plus jeune age, Education et implication à la citoyenneté
- Utlisation et philosophie du Libre et du Partage (Logiciel Libre, Wikipedia..)

## Proverbe
personne n'éduque autrui, personne ne s'éduque seul, les hommes s'éduquent ensemble par l'intermédiaire du monde.
Paulo FREIRE (1974)
